<?php
// include a library
require(__DIR__."/vendor/autoload.php");

$dic = new Pimple;
$dic['QueueClient.url'] = "127.0.0.1:11300";

$dic['QueueClient'] = $dic->share(function () use ($dic) {
    return new Inanimatt\MessageQueue\BeanstalkClient(
        new Pheanstalk_Pheanstalk($dic['QueueClient.url'])
    );
});

$dic['QueueForwarder'] = $dic->share(function () use ($dic) {
    return new Inanimatt\MessageQueue\Forwarder($dic['QueueClient']);
});
$dic['QueueReceiver'] = $dic->share(function () use ($dic) {
    return new Inanimatt\MessageQueue\Receiver($dic['QueueClient']);
});


$test = array(
    'stuff' => 'things',
    'other things' => 'stuff',
);


$dic['QueueForwarder']->forward($test);
// echo "Sent message".PHP_EOL;

$message = $dic['QueueReceiver']->receive();
var_dump($message);

assert($message === $test);
