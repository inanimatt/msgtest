<?php
// include a library
require(__DIR__."/vendor/autoload.php");

$dic = new Pimple;
$dic['QueueClient.url'] = "tcp://localhost:61613";
$dic['QueueClient.username'] = "matt";
$dic['QueueClient.password'] = "password";


$dic['QueueClient'] = $dic->share(function () use ($dic) {
    return new Inanimatt\MessageQueue\StompClient(
        new FuseSource\Stomp\Stomp($dic['QueueClient.url']), 
        $dic['QueueClient.username'], 
        $dic['QueueClient.password']
    );
});

$dic['QueueForwarder'] = $dic->share(function () use ($dic) {
    return new Inanimatt\MessageQueue\Forwarder($dic['QueueClient']);
});
$dic['QueueReceiver'] = $dic->share(function () use ($dic) {
    return new Inanimatt\MessageQueue\Receiver($dic['QueueClient']);
});


$test = array(
    'stuff' => 'things',
    'other things' => 'stuff',
);


$dic['QueueForwarder']->forward($test);
// echo "Sent message".PHP_EOL;

$message = $dic['QueueReceiver']->receive();
var_dump($message);

assert($message === $test);
