<?php

namespace Inanimatt\MessageQueue;

use FuseSource\Stomp\Stomp;

class StompClient implements ClientInterface
{
    private $client;
    private $username;
    private $password;
    
    public function __construct(Stomp $client, $username, $password)
    {
        $this->client = $client;
        $this->username = $username;
        $this->password = $password;
    }
    
    public function connect()
    {
        return $this->client->connect($this->username, $this->password);
    }
    
    public function disconnect()
    {
        return $this->client->disconnect();
    }
    
    public function send(MessageInterface $message)
    {
        return $this->client->send($message->getQueueName(), serialize($message->getParameters()), $message->getHeaders());
    }
        
    public function receive($queueName)
    {
        $this->client->subscribe($queueName);
        $frame = $this->client->readFrame();
        if ($frame !== null) {
            $message = new Message($frame->headers['destination'], unserialize($frame->body), $frame->headers);
            $this->client->ack($frame);
            return $message;
        }
        
    }

}