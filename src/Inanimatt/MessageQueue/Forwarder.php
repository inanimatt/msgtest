<?php

namespace Inanimatt\MessageQueue;

class Forwarder
{
    private $client;
    
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
        $this->client->connect();
    }
    
    /**
     * Wrap the input as a Message and send it via the client
     */
    public function forward($input)
    {
        $message = new Message('/queue/test', $input);
        $this->client->send($message);
    }
    
    public function __destruct()
    {
        $this->client->disconnect();
    }
}
