<?php

namespace Inanimatt\MessageQueue;

interface ReceiverInterface
{
    public function receive();
}
