<?php

namespace Inanimatt\MessageQueue;

class Receiver
{
    private $client;
    
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
        $this->client->connect();
    }
    
    /**
     * Wrap the returned frame as a Message and acknowledge it
     */
    public function receive()
    {
        $message = $this->client->receive('/queue/test');
        return $message->getParameters();
    }
    
    public function __destruct()
    {
        $this->client->disconnect();
    }
}
