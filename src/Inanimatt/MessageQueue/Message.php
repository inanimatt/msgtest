<?php

namespace Inanimatt\MessageQueue;

class Message implements MessageInterface
{
    private $queueName;
    private $message;
    private $headers;
    
    public function __construct($queue, $message, $headers = null)
    {
        if (!$headers) {
            $headers = array();
        }
        
        $this->queueName = $queue;
        $this->message = $message;
        $this->headers = $headers;
        
    }


    public function getHeaders()
    {
        return $this->headers;
    }

    public function getParameters()
    {
        return $this->message;
    }

    public function getQueueName()
    {
        return $this->queueName;
    }
}
