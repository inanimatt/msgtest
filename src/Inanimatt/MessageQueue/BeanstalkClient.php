<?php

namespace Inanimatt\MessageQueue;

use \Pheanstalk_Pheanstalk;

class BeanstalkClient implements ClientInterface
{
    private $client;
    private $connection;

    public function __construct(Pheanstalk_Pheanstalk $client)
    {
        $this->client = $client;
    }
    
    public function connect()
    {
    }
    
    public function disconnect()
    {
    }
    
    public function send(MessageInterface $message)
    {
        return $this->client->useTube($message->getQueueName())->put(serialize($message->getParameters()));
    }
        
    public function receive($queueName)
    {
        $job = $this->client->watch($queueName)->ignore('default')->reserve();
        if ($job !== null) {
            $message = new Message($queueName, unserialize($job->getData()), null);
            $this->client->delete($job);
            return $message;
        }
        
    }

}