<?php

namespace Inanimatt\MessageQueue;

interface ClientInterface
{
    public function connect();
    public function disconnect();
    public function send(MessageInterface $message);
}