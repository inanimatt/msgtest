<?php

namespace Inanimatt\MessageQueue;

interface ForwarderInterface
{
    public function forward($message);
}
