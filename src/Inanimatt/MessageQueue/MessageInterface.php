<?php

namespace Inanimatt\MessageQueue;

interface MessageInterface
{
    public function getHeaders();
    public function getParameters();
    public function getQueueName();
}
